package com.classofjava.di;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.classofjava.di.model.Account;
import com.classofjava.di.service.AccountService;

public class ConsoleApp {
	public static void main(String[] args) throws Exception {
		ApplicationContext appCtx = new ClassPathXmlApplicationContext(
				"spring-di.xml");
		AccountService accountService = (AccountService) appCtx
				.getBean("accountService");
		List<Account> delinquentAccounts = accountService
				.findDeliquentAccounts();

		for (Account a : delinquentAccounts) {
			System.out.println(a.getAccountNo());
		}
		AbstractApplicationContext aac = (AbstractApplicationContext) appCtx;
		aac.registerShutdownHook();
	}
}
