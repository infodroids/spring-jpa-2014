package com.classofjava.di.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.classofjava.di.dao.AccountDao;
import com.classofjava.di.model.Account;

@Component
public class AccountService {
	
	@Autowired
	@Qualifier("jdbc")
    private AccountDao accountDao;

    public AccountService() {}
    
    @PostConstruct
    public void init() {
    	System.out.println("Called after construction");
    }

	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	public List<Account> findDeliquentAccounts() throws Exception {
		List<Account> delinquentAccounts = new ArrayList<Account>();		
		List<Account> accounts = accountDao.findAll();
		
		Date thirtyDaysAgo = daysAgo(30);
		for (Account account : accounts) {
			boolean owesMoney = account.getBalance()
				.compareTo(BigDecimal.ZERO) > 0; 
			boolean thirtyDaysLate = account.getLastPaidOn()
				.compareTo(thirtyDaysAgo) <= 0;
			 
			if (owesMoney && thirtyDaysLate) {
				delinquentAccounts.add(account);
			}
		}
		return delinquentAccounts;
	}
	
	private static Date daysAgo(int days) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.DATE, -days);
		return gc.getTime();		
	}	
	
	@PreDestroy
	public void cleanup() {
		System.out.println("Freeing up resources");
	}
        
}
