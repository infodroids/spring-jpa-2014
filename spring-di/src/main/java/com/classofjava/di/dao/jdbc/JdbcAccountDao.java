package com.classofjava.di.dao.jdbc;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.classofjava.di.dao.AccountDao;
import com.classofjava.di.model.Account;

@Component
@Qualifier("jdbc")
public class JdbcAccountDao implements AccountDao {

	@Autowired
	private DataSource dataSource;

	public JdbcAccountDao() {
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public List<Account> findAll() throws Exception {
		throw new UnsupportedOperationException(
				"This method has not been implemented");
	}

}
