package com.classofjava.di.dao;

import java.util.List;

import com.classofjava.di.model.Account;

public interface AccountDao {

	List<Account> findAll() throws Exception;
}
