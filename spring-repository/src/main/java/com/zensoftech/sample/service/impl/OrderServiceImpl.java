package com.zensoftech.sample.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zensoftech.sample.model.Customer;
import com.zensoftech.sample.model.Order;
import com.zensoftech.sample.model.OrderItem;
import com.zensoftech.sample.repository.CustomerRepository;
import com.zensoftech.sample.repository.OrderItemRepository;
import com.zensoftech.sample.repository.OrderRepository;
import com.zensoftech.sample.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderItemRepository orderItemRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Transactional
	public Order submit(Order order, int customerId, List<OrderItem> orderItems) {
		Customer customer = customerRepository.findOne(customerId);
		order.setCustomer(customer);
		orderRepository.save(order);
		for (OrderItem orderItem : orderItems) {
			orderItem.setOrder(order);
			orderItemRepository.save(orderItem);
			order.getOrderItems().add(orderItem);
		}
		return order;
	}

	public Order find(Integer ordId) {
		return orderRepository.findOne(ordId);
	}

}
