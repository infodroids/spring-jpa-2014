package com.zensoftech.sample.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zensoftech.sample.model.Product;
import com.zensoftech.sample.repository.ProductRepository;
import com.zensoftech.sample.service.ProductService;
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	public void create(Product product) {
		productRepository.save(product);
	}

	public Product find(int productId) {
		return productRepository.findOne(productId);
	}

}
