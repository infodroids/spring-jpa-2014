package com.zensoftech.sample.service;

import java.util.Set;

import com.zensoftech.sample.model.Article;
import com.zensoftech.sample.model.Customer;
import com.zensoftech.sample.model.Product;

public interface ArticleService {

	void createStoredArticle(byte[] content, Product product, Customer customer);

	void createReferenceArticle(String url, Product product, Customer customer);

	Set<Article> findArticle(Customer customer);

	Set<Article> findArticle(Product product);

	Set<Article> findArticle(Product product, Customer customer);

}
