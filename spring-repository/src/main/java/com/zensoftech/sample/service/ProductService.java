package com.zensoftech.sample.service;

import com.zensoftech.sample.model.Product;

public interface ProductService {
	
	public void create(Product product);
	
	public Product find(int productId);

}
