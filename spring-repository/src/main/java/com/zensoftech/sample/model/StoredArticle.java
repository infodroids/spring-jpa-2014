package com.zensoftech.sample.model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "StoredArticle")
@PrimaryKeyJoinColumn(name = "storedArticleId")
public class StoredArticle extends Article {

	@Lob
	private byte[] content;

	public StoredArticle() {
	}

	public StoredArticle(Product product, Customer customer, byte[] content) {
		super(customer, product);
		this.content = content;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

}
