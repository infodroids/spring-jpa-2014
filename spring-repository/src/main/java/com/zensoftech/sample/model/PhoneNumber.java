package com.zensoftech.sample.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PhoneNumber {

	@Column(name = "NumberType")
	private String numberType;

	@Column(name = "CountryCode")
	private Short countryCode;

	@Column(name = "StdCode")
	private Short stdCode;

	@Column(name = "Number")
	private Integer number;
	
	public PhoneNumber(){}

	public PhoneNumber(String numberType, Short countryCode, Short stdCode,
			Integer number) {
		super();
		this.numberType = numberType;
		this.countryCode = countryCode;
		this.stdCode = stdCode;
		this.number = number;
	}

	public Short getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(Short countryCode) {
		this.countryCode = countryCode;
	}

	public Short getStdCode() {
		return stdCode;
	}

	public void setStdCode(Short stdCode) {
		this.stdCode = stdCode;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

}
