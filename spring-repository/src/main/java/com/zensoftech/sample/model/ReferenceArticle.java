package com.zensoftech.sample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ReferenceArticle")
@PrimaryKeyJoinColumn(name = "referenceArticleId")
public class ReferenceArticle extends Article {

	@Column(name = "URL")
	private String url;

	public ReferenceArticle() {
	}

	public ReferenceArticle(Product product, Customer customer, String url) {
		super(customer, product);
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
