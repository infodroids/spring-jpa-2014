package com.zensoftech.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zensoftech.sample.model.Hobby;

public interface HobbyRepository extends JpaRepository<Hobby, Integer> {

}
