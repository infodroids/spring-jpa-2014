package com.zensoftech.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zensoftech.sample.model.Product;

public interface ProductRepository extends JpaRepository<Product, Integer>{

}
