package com.zensoftech.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zensoftech.sample.model.OrderItem;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {

}
