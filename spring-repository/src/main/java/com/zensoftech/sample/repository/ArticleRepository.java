package com.zensoftech.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zensoftech.sample.model.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

}
