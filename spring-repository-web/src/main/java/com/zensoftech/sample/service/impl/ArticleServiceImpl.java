package com.zensoftech.sample.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zensoftech.sample.model.Article;
import com.zensoftech.sample.model.Customer;
import com.zensoftech.sample.model.Product;
import com.zensoftech.sample.model.ReferenceArticle;
import com.zensoftech.sample.model.StoredArticle;
import com.zensoftech.sample.repository.ArticleRepository;
import com.zensoftech.sample.repository.CustomerRepository;
import com.zensoftech.sample.repository.ProductRepository;
import com.zensoftech.sample.service.ArticleService;

@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Transactional
	public void createStoredArticle(byte[] content, Product product,
			Customer customer) {
		Product prd = productRepository.findOne(product.getId());
		Customer cst = customerRepository.findOne(customer.getId());
		Article storedArticle = new StoredArticle(prd, cst, content);
		articleRepository.save(storedArticle);
	}

	@Transactional
	public void createReferenceArticle(String url, Product product,
			Customer customer) {
		Product prd = productRepository.findOne(product.getId());
		Customer cst = customerRepository.findOne(customer.getId());
		Article refArticle = new ReferenceArticle(prd, cst, url);
		articleRepository.save(refArticle);
	}

	public Set<Article> findArticle(Customer customer) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Article> findArticle(Product product) {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<Article> findArticle(Product product, Customer customer) {
		// TODO Auto-generated method stub
		return null;
	}

}
