package com.zensoftech.sample.service;

import org.springframework.data.domain.Page;

import com.zensoftech.sample.model.Product;

public interface ProductService {
	
	public void create(Product product);
	
	public Product find(int productId);

	public Page<Product> findAll(int pageNum, int defaultPageSize);

	public void deleteById(Integer id);

	public Product findById(Integer id);

	public void update(Product product);

}
