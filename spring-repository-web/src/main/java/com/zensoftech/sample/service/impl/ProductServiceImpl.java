package com.zensoftech.sample.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.zensoftech.sample.model.Product;
import com.zensoftech.sample.repository.ProductRepository;
import com.zensoftech.sample.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	public void create(Product product) {
		productRepository.save(product);
	}

	public Product find(int productId) {
		return productRepository.findOne(productId);
	}

	@Override
	public Page<Product> findAll(final int pageNum, final int defaultPageSize) {
		return productRepository.findAll(new PageRequest(pageNum,
				defaultPageSize));
	}

	@Override
	public void deleteById(Integer id) {
		productRepository.delete(id);
	}

	@Override
	public Product findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Product product) {
		// TODO Auto-generated method stub

	}

}
