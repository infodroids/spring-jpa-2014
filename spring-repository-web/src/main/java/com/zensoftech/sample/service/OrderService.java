package com.zensoftech.sample.service;

import java.util.List;

import com.zensoftech.sample.model.Order;
import com.zensoftech.sample.model.OrderItem;

public interface OrderService {

	public Order submit(Order order, int customerId, List<OrderItem> orderItems);

	public Order find(Integer ordId);

}
