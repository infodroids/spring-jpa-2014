package com.zensoftech.sample.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zensoftech.sample.model.Customer;
import com.zensoftech.sample.model.Hobby;
import com.zensoftech.sample.repository.CustomerRepository;
import com.zensoftech.sample.repository.HobbyRepository;
import com.zensoftech.sample.service.CustomerService;

@Service
public class CustomerSerivceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepsoitory;

	@Autowired
	HobbyRepository hobbyRepository;

	public void create(Customer customer) {
		customerRepsoitory.save(customer);
	}

	public Customer find(int customerId) {
		return customerRepsoitory.findOne(customerId);
	}

	@Transactional
	public void createHobbiesForCustomer(Customer customer,
			Set<Hobby> customerHobbies) {
		List<Hobby> hobbies = hobbyRepository.findAll();
		Customer findOne = customerRepsoitory.findOne(customer.getId());
		findOne.getHobbies().addAll(hobbies);
		for (Hobby hobby : hobbies) {
			hobby.getCustomers().add(customer);
		}
	}

	@Transactional
	public Set<Hobby> createAndReturnHobbies(List<Hobby> hobbies) {
		hobbyRepository.save(hobbies);
		HashSet<Hobby> hashSet = new HashSet<Hobby>();
		for (Hobby hob : hobbies) {
			hashSet.add(hob);
			hob.getCustomers().size();
		}
		return hashSet;
	}

}
