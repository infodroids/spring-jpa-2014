package com.zensoftech.sample.service;

import java.util.List;
import java.util.Set;

import com.zensoftech.sample.model.Customer;
import com.zensoftech.sample.model.Hobby;

public interface CustomerService {

	public void create(Customer customer);
	
	public Set<Hobby> createAndReturnHobbies(List<Hobby> hobbies);

	public void createHobbiesForCustomer(Customer customer, Set<Hobby> hobbies);

	public Customer find(int customerId);

}
