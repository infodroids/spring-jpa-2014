package com.zensoftech.sample;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zensoftech.sample.model.Customer;
import com.zensoftech.sample.model.Hobby;
import com.zensoftech.sample.model.Order;
import com.zensoftech.sample.model.OrderItem;
import com.zensoftech.sample.model.PhoneNumber;
import com.zensoftech.sample.model.Product;
import com.zensoftech.sample.model.ValuedCustomer;
import com.zensoftech.sample.service.ArticleService;
import com.zensoftech.sample.service.CustomerService;
import com.zensoftech.sample.service.OrderService;
import com.zensoftech.sample.service.ProductService;

public class Main {

	private static Logger LOG = LoggerFactory.getLogger(Main.class);

	public static void main(String ar[]) {

		ApplicationContext appCtx = new ClassPathXmlApplicationContext(
				"META-INF/applicationContext.xml");

		CustomerService custService = appCtx.getBean(CustomerService.class);
		Customer customer = new Customer("Mark", "Twain", "mark@zensoftech.com");
		customer.getPhoneNumbers().add(
				new PhoneNumber("Cell", (short) 1, null, 909099911));
		customer.getPhoneNumbers().add(
				new PhoneNumber("Home", (short) 1, (short) 222, 9090999));

		custService.create(customer);

		Customer customer1 = new ValuedCustomer("John", "Smith",
				"john@zensoftech.com", (short) 5);

		custService.create(customer1);

		ProductService prdService = appCtx.getBean(ProductService.class);
		Product product = new Product(null, "IPod", "Apple device");
		prdService.create(product);

		Customer cust = custService.find(customer.getId());
		Product prd = prdService.find(product.getId());

		OrderService ordService = appCtx.getBean(OrderService.class);
		OrderItem ordItem = new OrderItem(null, 2, new BigDecimal(555.00), prd);

		List<OrderItem> orderItems = new ArrayList<OrderItem>();
		orderItems.add(ordItem);
		Order ord = ordService
				.submit(new Order(null), cust.getId(), orderItems);

		Order placedOrder = ordService.find(ord.getId());
		LOG.info(placedOrder.toString());

		Hobby hobby1 = new Hobby(null, "Music");
		Hobby hobby2 = new Hobby(null, "Art");
		Hobby hobby3 = new Hobby(null, "Reading");

		List<Hobby> hobbies = new ArrayList<Hobby>();
		hobbies.add(hobby1);
		hobbies.add(hobby2);
		hobbies.add(hobby3);
		Set<Hobby> allHobbies = custService.createAndReturnHobbies(hobbies);

		custService.createHobbiesForCustomer(cust, allHobbies);

		ArticleService artService = appCtx.getBean(ArticleService.class);
		artService.createStoredArticle("good".getBytes(), product, customer1);
		artService.createReferenceArticle("http://www.olx.com/article1",product, customer);

		AbstractApplicationContext ctx = (AbstractApplicationContext) appCtx;
		ctx.registerShutdownHook();

	}

}
