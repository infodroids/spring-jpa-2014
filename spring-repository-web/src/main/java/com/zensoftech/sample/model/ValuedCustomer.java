package com.zensoftech.sample.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "VC")
public class ValuedCustomer extends Customer {

	@Column(name = "Rating")
	private Short rating;
	
	public ValuedCustomer() {}

	public ValuedCustomer(Short rating) {
		super();
		this.rating = rating;
	}

	public ValuedCustomer(String firstName, String lastName, String email,
			short rating) {
		super(firstName, lastName, email);
		this.rating = rating;
	}

	public Short getRating() {
		return rating;
	}

	public void setRating(Short rating) {
		this.rating = rating;
	}

}
