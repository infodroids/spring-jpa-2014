package com.zensoftech.sample.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "Customer")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "CutomerType", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "C")
@NamedQuery(name = "Customer.findByCustomerIdWithOrders", query = "select c from Customer c join c.orders o where c.id = :id")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer id;

	@Column(name = "FirstName", length = 512)
	private String firstName;

	@Column(name = "LastName", nullable = false, length = 512)
	private String lastName;

	@Column(name = "Email", nullable = false, length = 512)
	private String email;

	@OneToMany(mappedBy = "customer")
	private Set<Order> orders;

	@ElementCollection
	@CollectionTable(name = "Customer_PhoneNumber", joinColumns = { @JoinColumn(name = "FK_Customer_Id") })
	private Set<PhoneNumber> phoneNumbers;

	@ManyToMany
	@JoinTable(name = "Customer_Hobby", joinColumns = @JoinColumn(name = "FK_Customer_Id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "FK_Hobby_Id", referencedColumnName = "id"))
	private Set<Hobby> hobbies;

	public Customer() {
	}

	public Customer(String firstName, String lastName, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Order> getOrders() {
		return orders;
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}

	public Set<Hobby> getHobbies() {
		if (hobbies == null) {
			hobbies = new LinkedHashSet<Hobby>();
		}
		return hobbies;
	}

	public Set<PhoneNumber> getPhoneNumbers() {
		if (phoneNumbers == null) {
			phoneNumbers = new LinkedHashSet<PhoneNumber>();
		}
		return phoneNumbers;
	}

	public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public void setHobbies(Set<Hobby> hobbies) {
		this.hobbies = hobbies;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + "]";
	}

}
