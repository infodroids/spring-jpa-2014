package com.zensoftech.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zensoftech.sample.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{

}
