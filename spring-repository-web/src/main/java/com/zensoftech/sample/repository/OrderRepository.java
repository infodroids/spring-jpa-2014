package com.zensoftech.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zensoftech.sample.model.Order;

public interface OrderRepository extends JpaRepository<Order, Integer>{

}
