package com.zensoftech.sample.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.zensoftech.sample.model.Product;
import com.zensoftech.sample.service.ProductService;

@Controller("productContoller")
@RequestMapping("/product")
public class ProductController {

	protected static final Logger LOGGER = LoggerFactory
			.getLogger(ProductController.class);

	protected static final int DEFAULT_PAGE_NUM = 0;
	protected static final int DEFAULT_PAGE_SIZE = 5;

	public ProductController() {
		LOGGER.info("Product controller init !!");
	}

	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public @ModelAttribute
	Product create(Model model) {
		Product product = new Product();
		return product;
	}

	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String createOnSubmit(@Valid Product product,
			BindingResult bindingResult, Model model) {
		productService.create(product);
		return "redirect:/product/list";
	}

	@RequestMapping(value = "/list")
	public String list(
			@RequestParam(value = "page", required = false) Integer page,
			Model model) {
		int pageNum = page != null ? page : DEFAULT_PAGE_NUM;
		Page<Product> paging = productService.findAll(pageNum,
				DEFAULT_PAGE_SIZE);
		model.addAttribute("page", paging);
		return "/product/list";
	}

	@RequestMapping(value = "/delete/{id}")
	public String delete(
			@RequestParam(value = "page", required = false) Integer page,
			@PathVariable("id") Integer id) {
		LOGGER.debug("delete id={}", id);
		productService.deleteById(id);

		return "redirect:/product/list";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") Integer id, Model model) {
		Product product = productService.findById(id);
		model.addAttribute(product);
		return "/product/form";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String editOnSubmit(@Valid Product product,
			BindingResult bindingResult, Model model) {
		LOGGER.debug("edit person={}", product);
		if (bindingResult.hasErrors()) {
			LOGGER.warn("validation error={}", bindingResult.getModel());
			model.addAllAttributes(bindingResult.getModel());
			return "/product/form";
		}
		productService.update(product);
		return "redirect:/product/list";
	}

}
