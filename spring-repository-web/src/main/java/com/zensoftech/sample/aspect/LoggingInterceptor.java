package com.zensoftech.sample.aspect;

import org.aspectj.lang.JoinPoint;

public class LoggingInterceptor {

	public LoggingInterceptor() {
		System.out.println("Init Aspect");
	}

	public void logBeforeCreateProduct(JoinPoint jp) {
		Object[] args = jp.getArgs();
		System.out.println("**********" + args);
	}

}
